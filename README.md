# ADA open End
NAME- AMAN JAISWAL
USN - 1SI18CS010

Given two strings find the Longest Common Subsequence. For example, if X is
CTGCTGA and Y is TGTGT, then the longest subsequence is TGTG. Design and
implement the solution to the above problem.


The solution is such that the main.cpp contains the main function which will ask
for the two strings in which we want to find the longest common sequence which
will call the function from fn.h which will find the longest subsequence of the
two input strings and send the result back to main function and then the resulted 
string will be displayed