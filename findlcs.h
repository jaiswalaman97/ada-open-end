#include<bits/stdc++.h>
using namespace std;

string findlcsstring(int m,int n,vector<vector<int>> table,string s1,string s2)
{
  int index=table[m][n];
  char lcs[index+1];    //To store the lcs string
  lcs[index]='\0';
  int i=m,j=n;    //We start from the right most bottom most in the table
  while(i>0 && j>0)
  {
    if(s1[i-1]==s2[j-1])
    {
      lcs[index-1]=s1[i-1];   //save the common character in the lcs string
      i--;
      j--;
      index--;
    }
    //If the character in both the strings are not equal the we go for the max adjacent value
    else if(table[i-1][j]>table[i][j-1])
      i--;
    else
      j--;
  }
  return lcs;
}
