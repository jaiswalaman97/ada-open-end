#include<bits/stdc++.h>
#include"findlcs.h"
using namespace std;
string LCS(string s1,string s2)
{
  int m=s1.length(),n=s2.length();
  vector< vector<int> > table(m+1);
  // Creating the length table
  for(int i=0;i<=m;i++)
  {
    table[i].resize(n+1);
    for(int j=0;j<=n;j++)
    {
      if(i==0 || j==0)
        table[i][j]=0;
      else if(s1[i-1]==s2[j-1])
        table[i][j]=table[i-1][j-1]+1;
      else
        table[i][j]=max(table[i-1][j],table[i][j-1]);
    }
  }
  return findlcsstring(m,n,table,s1,s2);
}
