#include<bits/stdc++.h>
#include"lcs.h"
using namespace std;

int main()
{
  string s1,s2,s3;
  cout<<"Enter the first string: ";
  cin>>s1;
  cout<<"Enter the second string: ";
  cin>>s2;
  s3=LCS(s1,s2);
  cout<<"Longest common sequence: "<<s3<<endl;
  return 0;
}
